% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\usepackage[english,serbian]{babel}

\usepackage[unicode]{hyperref}
\usepackage[font=small,labelfont=bf]{caption}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\newtheorem{primer}{Primer}[section]

\begin{document}

%% TODO: dati kraci i precizniji naziv radu
\title{Standardi verifikacije avionskog i svemirskog softvera\\ \vspace{0.1cm} \small{Seminarski rad u okviru kursa\\Metodologija stručnog i naučnog rada\\ Matematički fakultet}}

\author{Irena Blagojević, Gorana Vučić, Nikola Katić\\{\small 9irenab@gmail.com, goranavucic94@gmail.com, nikolakatic@rocketmail.com }}
\date{\today}
\maketitle

\vspace{-0.2805cm} %% HACK: inace literatura pada na drugu stranu

\abstract{
  Pred softver koji se koristi u avionskoj i svemirskoj industriji postavljeni su najrigorozniji zahtevi u pogledu bezbednosti. Ustaljeni procesi dizajna i razvoja softvera koji važe u većem delu industrije sami po sebi nisu zadovoljavajući. Poželjno je, a negde i neophodno iskoristiti znanje i tehnike iz akademskih krugova. Formalne metode imaju važnu ulogu u verifikaciji bezbednosno-kritičnog softvera. Teoretski aspekt nije objašnjavan, za više detalja su date reference. U ovom radu su opisani i neki od standarda koji se koriste u avionskoj i svemirskoj industriji, sa osvrtom na proces verifikacije softvera, tehnike i alate koji se u tom procesu koriste.

{\bf{Ključne reči:}} verifikacija softvera, avionska i svemirska industrija, standardi verifikacije, formalna verifikacija, bezbednosno-kritični sistemi
\setcounter{tocdepth}{2}
\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}

Specifični sistemi kod kojih greške u softveru prouzrokuju posledice koje dovode u pitanje bezbednost ljudi, nazivaju se bezbednosno-kritičnim sistemima (eng.~{\em safety critical systems}, u nastavku teksta, kritični sistemi). Primeri takvih sistema su fabrička i nuklearna postrojenja, aparati koji se koriste u medicinske svrhe, sistemi za kontrolu leta \cite{rigSoftDev}. Podaci koje NIST (eng.~{\em{National Institute of Standards and Technology}}) iznosi u \cite{NIST} pokazuju da se oko 70\% defakata uvodi u fazama dizajniranja i kroz zahteve \cite{FTAforAviationSoft}.

Veoma je teško uveriti se u apsolutnu korektnost programa, ali iako takav cilj može biti nedostižan, povećanje stepena pouzdanosti je opravdana i centralna tema izrade softvera kritičnih sistema. Uobičajeni pristup provere softvera se vrši testiranjem i simulacijom. Taj način ipak nije u potpunosti zadovoljavajuć jer velika većina sistema ima neizmerno veliki prostor vrednosti. Testiranje svih putanji u tom slučaju nema praktični značaj.{~\em{Formalne metode}}\footnote{Podrazumeva matematičko modelovanje, predviđanje, dizajniranje i analizu sistema i softvera. Detaljnije o tome u \cite{rigSoftDev}. } predstavljaju alternativu jer pomoću matematičkog aparata nude mogućnost zaključivanja o {\em{celom prostoru vrednosti i skupu parametara}} nad kojim softver obavlja svoju funkciju -- nasuprot testiranju, pomoću kojeg se zaključivanja vrše na osnovu pojedinačnih uzoraka \cite{rigSoftDev, staticanlyzLargeSafetyCritical}.

Kada se govori o pouzdanosti, robusnosti i tačnosti softvera, neophodno je znati i koji su zahtevi koji se nameću. Zahtevi se mogu podeliti na funkcionalne i nefunkcionalne. Prvi odgovaraju na pitanje {\em{šta}} sistem radi i na taj način pružaju vezu između ulaza i izlaza programa. Druga grupa zahteva daje odgovor na pitanje {\em{kako}} sistem funkcioniše u smislu efikasnosti, kvaliteta usluge (eng.~{\em{quality of service}}) i održavanja. Posebna klasa zahteva koja se odnosi na sigurnost (eng.~{\em{safety issues}}) pripada funkcionalnim zahtevima \cite{rigSoftDev}, a u nastavku rada biće detaljnije opisana.

\section{Verifikacija bezbednosno-kritičnog softvera}
\label{sec:bezbednosnoKriticniSoftver}

Softver kritičnih sistema ne sme da otkaže, a u odsustvu formalne specifikacije teško je objasniti šta se podrazumeva pod neuspehom \cite{staticanlyzLargeSafetyCritical}. Izuzev opasnosti koje prete zbog grešaka u samom sistemu, poseban osvrt u toku analize zaslužuju i spoljašnji uslovi \cite{FTAforAviationSoft}. U specifikacijama kritičnih sistema prisutne su naredne dve stavke. Jedna podrazumeva nemogućnost izvršavanja instrukcije koja ima {\em{nedefinisano}} ponašanje, a druga da softver ne izvršava instrukciju koja prouzrokuje {\em{fatalnu grešku}} (eng.~{\em{fatal error}}). Nepravilno korišćenje aritmetičkih operacija, prekoračenje opsega, deljenje sa nulom i nedozvoljen pristup memorije su neki od uzročnika fatalnih grešaka \cite{staticanlyzLargeSafetyCritical}. U nastavku će biti opisane formalne metode koje se koriste u razvoju softvera za letilice, mogućnosti alata Astr\' ee i neki primeri upotrebe alata formalne verifikacije u avionskoj i svemirskoj industriji.

\subsection{Formalne metode}
\label{subsec:formalneMetode}

%% U ovoj sekciji biće navedenn formalne metode koje se koriste u razvoju softvera za letilice, i prikaz mogućnosti alata Astree.

Pojam {\em{specifikacije}} u kontekstu formalnih metoda podrazumeva model sistema koji sadrži opis željenog ponašanja. U tom smislu se nameću dva osnovna problema. Kako na nivou specifikacije podstaći željeno ponašanje se naziva problemom {\em{validacije modela}}. Kako na osnovu specifikacije dobiti implementaciju sa istim ponašanjem, ili posmatrano iz drugog ugla, na koji način se za datu implementaciju može tvrditi da ima isto ponašanje kao i specifikacija? Prethodno pitanje nazivamo problemom {\em{formalnog odnosa između specifikacija i implementacija}}.
Različite familije formalnih metoda daju veliki broj pristupa rešavanju pomenuta dva problema \cite{rigSoftDev}.

Postojeće tehnike formalnog dokazivanja su {\em{provera modela}} (eng.~{\em{Model-Checking}}), {\em{dokazivanje teorema}} (eng.~{\em{Theorem Proving}}) i {\em{apstraktna interpretacija}} (eng.~{\em{Abstract Interpretation}}) \cite{Astree}. Više informacija o navedenim tipovima moguće je pronaći u \cite{rigSoftDev}. % TODO: dodati jos neke reference ovde....

Prilikom dokazivanja bezbednosti u kritičnim sistemima, statički analizatori mogu dati doprinos ciljajući specifičnu klasu svojstava. Primeri svojstava su račun pokretnog zareza (eng.~{\em{Floating-point calculus}}), analiza steka, WCET (eng.~{\em{Worst-Case Execution Time}}), greške tokom izvršavanja (u nastavku RTE, što je akronim od eng.~{\em{Run Time Error}}), iskorišćenost memorije. Apstraktna interpretacija zasnovana na statičkoj analizi dobro skalira i njenim korišćenjem je moguće proveriti kritičan program u celini. Ovakvi analizatori predstavljaju dobar početni korak ka dokazivanju skoro svih bezbednosnih svojstava softvera. Naredni, veliki korak bio bi dokazivanje svojstava koje definiše korisnik \cite{Astree}.

\subsection{Astr\' ee}
\label{subsec:astree}

Na primeru alata Astr\' ee, dodatno će biti pojašnjen koncept statičkog analizatora. Ovaj alat koristi tehniku apstraktne interpretacije s ciljem dokazivanja nepostojanja RTE u programima napisanih u programskom jeziku C. Postoji određena klasa\footnote{Sinhroni programi imaju svojstvo da iako mogu biti sastavljeni od više paralelnih podzadataka, njihova serijalizacija je određena u vreme dizajna \cite{Astree}.} C programa za koju je Astr\' ee prvenstveno namenjen, i kada se nad njima koristi, daje znatno preciznije rezultate od statičkih analizatora opšte namene \cite{Astree}.

Prva apstrakcija koju Astr\' ee implementira je aproksimacija nadskupa svih dostižnih stanja programa, bez skladištenja nizova tih stanja. Intuitivno, stanje programa je uređeni par $(programska$ $lokacija$, $memorijsko$ $stanje)$, a apstraktnom invarijantom nazivamo skup svih lokalnih invarijanti programa. Lokalna invarijanta je definisana za određenu lokaciju i predstavlja procenu aproksimacije nadskupa svih mogućih memorijskih stanja. Kada je poznato na kojim svim lokacijama može doći do RTE, dijagnostički deo Astr\' ee na tim lokacijama proverava da li je RTE uslov zadovoljen lokalnom invarijantom \cite{Astree}.


Znajući da se posmatra nadskup svih putanji programa, kao i da pojedini RTE uslovi poput deljenja sa nulom mogu biti zadovoljeni i u situacijama koje nisu moguće u realnosti, ostavlja se prostor lažnim upozorenjima. Ne postoji jednostavan način da se utvrdi da li je upozorenje lažno. Za lokacije gde postoji upozorenje, neophodno je dodatno analizirati da li je RTE moguć u realnom scenariju. Krajnji ishod je utvrđivanje da program ne sadrži upozorenje koje nije lažno. Problem sa ovim pristupom je ljudski faktor i činjenica da je dodatna provera podložna greškama, a i oduzima vreme. Za jedan analizator se kaže da je precizniji od drugog ukoliko za isti program daje manje lažnih upozorenja \cite{Astree}.

%% U nastavku, biće spomenuti još neki od alata poput Frama-C, Caveat, aiT, StackAnalyzer, koji se takođe koriste tokom razvoja ovog tipa kritičnog softvera su.

\subsection{Primeri upotreba alata formalne verifikacije}
\label{subsec:primeriUpotrebaAlata}

NASA je 1999. godine razvila sistem baziran na veštačkoj inteligenciji Remote Agent, koji je bio uspešno demonstriran u toku leta {\em{Deep Space 1} -- DS-1}. Koristio je SPIN softver za verifikaciju (provera modela), zbog čega je razvijen alat Java PathFinder, koji direktno prevodi Java u program napisan u PROMELA jeziku, koji SPIN koristi. Misija DS-1 je bila prva koja je koristila veštačku inteligenciju na operativnom nivou tokom leta u svemiru. Više o korišćenju formalnih metoda u DS-1 se može naći u \cite{formalAnalysisRA}.

ESA-ina raketa-nosač Ariane 5 koristi dokazivače teorema (kao što je PVS (eng.~{\em{Prototype Verification System}})) i alate za proveru modela (kao što je Mur{\o}) kako bi se izvršila verifikacija da nema kršenja željenih svojstava modela sistema \cite{historyofbugs}.

Airbus kompanija na više od 10 projekata za A380 i A400M letelice koristi Stack Analyzer.
Ovaj alat analizira program tako što izračunava gornju granicu količine memorije koju program koristi, što doprinosi dokazivanju da nijedno izvršavanje programa neće dovesti do prekoračenja steka \cite{ASP}. 

aiT alat je originalno dizajniran u tesnoj saradnji sa Airbus kompanijom, gde se koristi već petnaest godina unazad. Alat se koristi za potvrđivanje vremenskog ponašanja kritičnog softvera, uključujući i softver za kontrolu leta A380 \cite{ait}.


\subsection{Standardi kritičnih sistema}
\label{subsec:standardiKriticnihSistema}

U različitim sektorima industrije kritičnih sistema poput železnica, vojne ili avionske industrije, postoji potreba regulisanja razvojnog procesa u saradnji sa standardizacionim telima. Takvi standardi propisuju definiciju pouzdanosti, pružajući sigurnosnu skalu (eng.~{\em{safety scale}}) namenjenu konkretnoj industrijskoj oblasti. Svakom nivou bezbednosti takve skale pridružen je scenario kojem pogoduje toliki stepen sigurnosti \cite{rigSoftDev}.

Kako bi se sistemu, ali i razvojnom procesu dodelila mera sigurnosti (tj. određen nivo na skali bezbednosti), predloženi standardi definišu i evaluacioni sistem i metriku, kao i validacioni i sertifikacioni proces. Da bi kritičan sistem bio saglasan sa određenim nivoom, procesi validacije i verifikacije moraju biti sprovedeni. Na kraju, sistem mora biti i sertifikovan od strane sertifikacionog tela, tj. validacioni i verifikacioni procesi takođe moraju biti potvrđeni \cite{rigSoftDev}.

%TODO: dodati i ostale standarde o kojima ce biti reci kasnije
U oblasti avionske i svemirske industrije ističe se nekoliko standarda koji su razmatrani u poglavljima \ref{sec:avio} i \ref{sec:svemir}.

\subsection{Tehnike analize i procene sigurnosti}
\label{subsec:tehnikeAnalizeIProceneSigurnostigs}

Prethodno pomenuti standardi koriste različite procedure i tehnike koje garantuju saglasnost sa bezbednosno-kritičnim zahtevima. Navodimo %obracanje u prvom licu mnozine??
primere tehnika koje se koriste u standardima avionske i svemirske industrije.

\begin{description}
\item[SFMECA](eng.~{\em{Software Failure Modes\footnote{Problematične putanje izvršavanja softvera.}, Effects and Criticality Analysis}}) podrazumeva identifikaciju funkcija komponenti kao početni korak i predviđanje uticaja na funkcionisanje celog sistema. Pristup odozdo-nagore se izvodi hijerarhijski. Efekti identifikovani na nižim nivoima se propagiraju ka višim \cite{rigSoftDev, FMbasedSoftReading}. Sveobuhvatniji način razmišljanja se naziva FMEA (eng.~{\em{Failure Modes and Effects Analisys}}). Slabosti ovakvog pristupa su slučajevi višestrukih otkazivanja i greške usled ljudskog faktora \cite{FTAforAviationSoft, sysSafetyFor21st}. % Dalje reference i više o ovoj familiji tehnika u \cite{sysSafetyFor21st, FMbasedSoftReading}.
\item[FTA] (eng.~{\em{Fault Tree Analysis}}) je statička tehnika analize pogodna za modelovanje, vizuelni prikaz i evaluaciju neuspešnih putanja (eng.~{\em{failure paths}}) sistema \cite{FTAdef, FTAforAviationSoft}. Alternativna definicija, data u \cite{FThandbook}, opisuje FTA kao pristup deduktivne analize u kojem se na najvišem nivou postulira incident, a zatim se traže problematične putanje izvršavanja, problematični događaji, kao i ponašanje komponenti koje doprinosi incidentu \cite{FTAforAviationSoft}. Na taj način se identifikuju kombinacije uslova koji bi prouzrokovale ulazak sistema u određeno opasno stanje. Primenom FTA u fazi kreiranja zahteva, olakšava se pronalaženje slabosti specifikacije i identifikovanje skupa zahteva koji može imati uticaj na kvalitet sistema u smislu sigurnosti. Ukoliko se FTA primeni tokom faze dizajniranja, omogućava se lakše uočavanje slabosti dizajna na viskom nivou identifikovanjem komponenti i modula koji imaju direktan uticaj na kvalitet softvera. Neki od alata koji podržavaju ovaj koncept su Saphire \cite{saphire}, FaultTree+ \cite{faultTree+}, OpenFTA \cite{openFTA}, BlockSim7 \cite{blockSim}. Detaljnije o FTA tehnici u \cite{FTAforAviationSoft, handbookOfRAMS}.
\item[ETA] (eng.~{\em{Event Tree Analysis}}) je induktivna logička metoda kojom se identifikuje niz slučajeva i nesreća koje mogu biti generisane od početnog, inicirajućeg događaja \cite{handbookOfRAMS}. ETA se može konvertovati u FTA, a procedure i formule koje se koriste prilikom kvantifikacije su slične. Osnovna razlika je u načinu razvijanja stabla, a samim tim i u broju čvorova koje je neophodno obići. ETA se razvija odozdo-nagore, pretražujući i neuspešne i uspešne putanje, dok se kod FTA vrši samo pretraga neuspešnih \cite{sysSafetyFor21st}. Detaljnije o ETA tehnici u \cite{handbookOfRAMS}.
\item[CCFA] (eng.~{\em{Common Cause Failure Analysis}}) tehnika koja se primarno koristi za evaluaciju višestrukih otkazivanja u sistemu nastalih zbog zajedničkog uzročnog faktora \cite{sysSafetyFor21st}. Detaljnije o CCFA tehnici u \cite{softwareCCA}.
\end{description}

Navedene tehnike pripadaju širem skupu metoda pod akronimom RAMS (eng.~{\em{Reliability, Availability, Maintainability and Safety}}) \cite{rigSoftDev, handbookOfRAMS}.

\section{Standardi u avionskoj industriji}
\label{sec:avio}

Bezbednosno-kritični ugrađeni softveri koji se razvijaju u avionskoj industriji su primer softvera gde jedna greška u razvoju može odneti živote stotine ljudi. Neki primeri nesreća su pad Airbus A400M aviona koji se dogodio 2015. godine u maju mesecu. Softverski problem prouzrokovao je grešku u kontrolnim jedinicama tri od četiri motora, i na taj način usmrtio četiri člana posade. Još jedan primer se desio 14. septembra 2004. godine na aerodromu u Los Anđelesu, kada je otkazao radio sistem za kontrolu leta. Usled greške prekoračenja memorijskog bafera, sistem je bio izgašen na oko tri sata, što je dovelo do otkazivanja 150 letova koji su brojali oko 30000 putnika, i umalo nije došlo do dve katastrofalne avionske nesreće \cite{crash}. Kako ne bi došlo do ovakvih neželjenih posledica, regulatorna tela i avionska industrija su definisali stroge standarde za razvoj softvera. Standard koji se trenutno primenjuje je DO-178, dok njegov ekvivalent u Evropi je ED-12 \cite{CAS}. Ovim standardom nametnuta su stroga pravila u procesu razvoja softvera. Verifikacija iako čini samo jedan od procesa u razvoju, predstavlja jedan od najtežih zadataka. Standardom su definisani i nivoi kritičnosti kvara kategorisanih prema posledicama koje imaju na samu letelicu, posadu i putnike. Nivoi kritičnosti se mogu videti u tabeli ~\ref{tab:a}  \cite{TFV}.
 



\begin{table}[htb]
\centering
\small
\begin{tabular}{ |p{0.7cm}|p{2.5cm}|p{6cm}|  }

\hline
\multicolumn{3}{|c|}{ \textbf{Nivoi kritičnosti kvara}} \\
\hline
\textbf{Nivo} & \textbf{Stanje otkaza}  & \textbf{Opis}\\
\hline
A & Katastrofalan & Kvar može prouzrokovati pad letelice \\
\hline
B & Opasan  & Kvar u velikoj meri utiče na bezbednost i performanse \\
\hline
C & Značajan & Kvar je značajan, ali ima manji uticaj od prethodnog stanja \\
\hline
D   & Neznatan &  Kvar je primetan, ali ima manji uticaj u odnosu na prethodno stanje\\
\hline
E & Bez posledica & Kvar ne utiče na sigurnost rada letelice \\
\hline
\end{tabular}
\caption {Nivoi kritičnosti kvara}\label{tab:a}
\end{table}


Dve najpozantije verzije ovog standarda koje će biti dalje objašnjene u tekstu su DO-178B %% \cite{rtca1992}
i DO-178C%% \cite{rtca2011}
. Verzija DO-178B koja se primenjuje od 1992. godine pokazala se veoma uspešnom. Međutim uvođenjem ovog standarda, na proces verifikacije softvera se trošilo i do sedam puta više vremena nego na ostale procese u razvoju softvera \cite{CAS}. Verifikacija, kao što je propisano ovim standardom, izvodi se pregledanjem, analizom ili testiranjem. Prva dva metoda se sprovode od strane čoveka, dok se testiranje odnosi na višestruko izvršavanje programa koji je potrebno verifikovati, i proveravanje da li se dobijeni rezultati slažu sa očekivanim \cite{TFV,ASP}. Složenost avionskog softvera u poslednjoj deceniji je povećana do tačke gde se pokazalo da tehnike verifikacije propisane ovim standaradom nisu dovoljne. To je dovelo do toga da avionska industrija razmotri alternativne načine verifikacije tokom revizije DO-178B standarda. Novi standard koji je počeo da se primenjuje od 2011. godine, DO-178C koristi nove verifikacione tehnike zasnovane na formalnim metodama \cite{TFV}. 

Primena novih verifikacionih tehnika se pokazala veoma uspešnom i ekonomičnom u dve velike kompanije Airbus i Dassault-Aviation. Tehnike koje se primenjuju su kategorizovane kao: apstraktna interpretacija zasnovana na statičkoj analizi, dokazivanje teorema i proveravanje modela \cite{TFV,ASP}.

\subsection{Proces razvoja softvera}

Standard DB-178 ne propisuje tačno određeni razvojni proces, već samo identifikuje važne korake unutar razvojnog procesa i definiše glavne ciljeve za svaki od ovih koraka. Glavni procesi koji se identifikuju u okviru razvoja su (Slika \ref{tab:b}):
\begin{itemize}
\item Proces uspostavljanja softverskih zahteva koji razvija zahteve na visokom nivou HLR (eng.~{\em High Level Requirements})
\item Proces dizajniranja softvera koji od HLR-a razvija zahteve na niskom nivou LLR (eng.~{\em Low Level Requirements}) i arhitekturu softvera
\item Proces implementiranja softvera koji razvija izvorni kod na osnovu prethodno definisanih zahteva na niskom nivou i softverske arhitekture
\item Proces integracije softvera koji izvšni kod izvršava na podrazumevanom hardveru \cite{TFV,ASP, CAS}
%The software integration process loads executable object code into the target hardware for hardware/software integration (prevod ???)
\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\linewidth]{procesRazvoja2.PNG}
  \caption{Proces razvoja softvera}\label{tab:b}

\end{figure}


DO-178 zahteva izvršavanje procesa verifikacije, kako bi se pokazalo da izvršni program zadovoljava postavljene zahteve. Za određene zahteve, verifikacija, koja uključuje formalnu analizu, može direktno da se izršava nad binarnom reprezentacijom programa. Na primer, Airbus koristi alate zasnovane na formalnoj analizi kako bi izračunao gornju granicu vremena izvršavanja programa, kao i maksimalnu iskorišćenost steka. Za mnoge druge zahteve koji su u vezi sa tokovima podataka i funkcionalnim zahtevima, formalna verifikacija se može primeniti samo nad izvornim kodom. DO-178 standard dozvoljava ovakav pristup, pod uslovom da korisnik dokaže da svojstva koja važe na nivou izvornog koda, važe i na nivou objektnog koda \cite{TFV}. Ovo dokazivanje je u velikoj meri olakšano upotrebom odgovarajućih alata. Važno je uveriti se u ispravnost prevođenja izvornog koda u objektni od strane kompajlera. U osnovi postoje dva pristupa, jedan se zasniva na ispitivanju samog kompajlera, dok drugi pristup ispituje izlaz iz kompajlera. Drugi pristup obezbeđuje odgovarajući stepen sigurnosti kroz višestruke aktivnosti propisane DO-178 standardom, uključujući integraciju, testiranje i verifikaciju objektnog koda \cite{TFV,ASP}.

Verifikacija propisana ovim standardom se uglavnom zasniva na verifikaciji zahteva visokog nivoa ({\em HLR}) kao i zahteva na niskom nivou ({\em LLR}). Bilo da se sprovodi testiranjem ili tehnikama zasnovanim na formalnim metodama mora da obuhvati sve slučajeve. 
Da bi se primenila formalna verifikacija nad zahtevima usklađenosti ({\em na primer usklađenost između HLR i LLR zahteva}), potrebno je izraziti zahteve na formalnom jeziku a potom se mogu koristiti tehnike zasnovane na simboličkom izvršavanju kako bi se proverilo da li su istoimeni zahtevi ispunjeni \cite{TFV,ASP}.  

U okviru standarda definisani su i robusni zahtevi. Najvažniji među njima je da se ne smeju pojavljivati greške u toku izvršavanja programa, kao što su neinicijalizovani podaci, pristupanje elementima van granica, dereferenciranje null pokazivača i tako dalje. 
Formalne analize mogu da pomognu u pronalaženju ovakvih grešaka. Najpoznatije među njima su proveravanje modela i apstraktna interpretacija \cite{TFV}. 

\subsection{Analiza pokrivenosti}


DO-178 standard zahteva analizu pokrivenosti (eng.~{\em coverage analysis}) kako bi se osiguralo da je softver u dovoljnoj meri testiran. Analiza pokrivenosti obuhvata analizu pokrivenosti zahteva (eng.~{\em requirements-based analysis}) i analizu strukturalne pokrivenosti (eng.~{\em structural coverage analyses}). Analiza pokrivenosti zahteva, proverava da li su svi zahtevi verifikovani, dok analiza strukturalne pokrivenosti ima za cilj da otkrije nedostatke  u testiranim slučajevima, neadekvatnost u postavljenim zahtevima kao i pronalaženje stranog koda (eng.~{\em extraneous code}). Umesto analize strukturalne pokrivenosti koriste se četiri alternativne aktivnosti zasnovane na formalnim metodama, koje imaju za cilj da verifikuju iste ciljeve. Aktivnosti se karakterišu kao: pokrivenost (eng.~{\em cover}), kompletnost (eng.~{\em complete}), tok podataka (eng.~{\em dataflow}) i strani kod (eng.~{\em extraneous}) \cite{TFV,ASP}.

Pokrivenost osigurava da je svaki zahtev u dovoljnoj meri matematički proveren. Rezultati formalne verifikacije zavise od pretpostavki. Stoga je bitno osigurati da su sve pretpostavke jasne, poznate i opravdane \cite{TFV}. 

Kompletnost  otkriva nedostajuće ili nekompletne zahteve. Neophodne su dodatne aktivnosti kako bi se osiguralo da su svi zahtevi izraženi, odnosno da su navedena sva prihvatljiva ponašanja softvera. Ova aktivnost navodi da kompletnost skupa zahteva treba da se pokaže u odnosu na nameravanu funkciju. Prva vrsta funkcija govori da za sva ulazna stanja, moraju biti precizirana izlazna stanja, dok druga vrsta funkcija govori da za sva izlazna stanja, ulazna stanja moraju biti precizirana \cite{TFV}. 

Analiza toka podataka otkriva sve neželjene tokove podataka. Kako bi se uvidelo da u fazi implementiranja koda nisu proizvedene nekakve nepoželjne funkcionalnosti, potrebno je pokazati da ne postoje neželjene zavisnosti između ulaznih i izlaznih stanja. Kako bi se ovo postiglo koristi se formalna analiza \cite{TFV}. 

Poslednja u nizu aktivnosti koja zamenjuje strukturalnu analizu je detekcija stranog koda, odnosno pronalaženje koda koji nije u vezi ni sa jednim postavljenim zahtevom. U ovu vrstu koda spada i mrtav kod (eng.~{\em dead code}). U ovoj aktivnosti ne koriste se alati zasnovani na formalnoj analizi jer se nisu pokazali adekvatnim, već se primenjuju tehnike pregledanja i analize koda. Napor koji je potrebno uložiti da bi se pregledanjem ili analizom utvrdilo da li postoji strani kod, zavisi od rezultata dobijenih sporvođenjem prethodne tri aktivnosti \cite{TFV}.

\subsection{Formalna verifikacija funkcionalnih zahteva}

Jedinično dokazivanje (eng.~{\em Unit proof}) se koristi za verifikaciju funkcionalnih zahteva koji se odnose na zahteve niskog nivoa. Prethodna tehnika koja se koristila u ove svrhe je testiranje jedinica koda (eng.~{\em Unit testing}). Jedinično dokazivanje se može sprovesti korišćenjem Caveat alata \cite{caveat} tako što se tokom razvojnog procesa u okviru dizajniranja softvera zahtevi definisani na niskom nivou zapisuju na formalnom jeziku. Nakon što se u fazi implementiranja softvera napiše određeni modul u C programskom jeziku, formalni zahtevi ovog modula kao i sam modul se dokazuju uz pomoć ovog alata. Postupak se ponavlja za svaku funkciju napisanu na C programskom jeziku svakog modula \cite{ASP}.

%% Analiza najgoreg vremena izvršavanja (eng.~{\em Worst Case Execution Time analysis}). Kod sistema u realnom vremenu od suštinskog značaja je da se program izvrši na vreme. Kako bi se osigurala ispravnost sistema, svaki program mora biti završen u određenom vremenskom okviru. Isto važi i za programe koji se izvršavaju u okviru avionskog softvera. Kako bi se izračunalo najgore vreme izvršavanja koristi se alat aiT, koji koristi apstraktnu interpretaciju zasnovanu na statičkoj analizi \cite{ASP}.

%% Izračunavanje maksimalne iskorišćenosti steka (eng.~{\em Maximum stack usage computation}). Za svaki zadatak u okviru programa avionskog softvera unapred je statički određena količina memorije. Ako bilo koji zadatak zahteva više memorije od one koja mu je unapred dodeljena, tokom izvršavanja programa pojavio bi se izuzetak prekoračenja steka. Kako bi se ovakvi problemi izbegli, unapred mora biti izračunata gornja granica iskorišćenosti svakog steka. Za ovakve svrhe koristi se alat Stackanalyzer \cite{stackanalyzer,ASP}}.

\section{Standardi svemirske industrije}
\label{sec:svemir}

U okviru ovog poglavlja biće opisani neki standardi koji se koriste u svemirskoj industriji, a koji se tiču razvoja softvera i osiguravanja softvera (eng.~{\em{software assurance}}). Standardi koji će biti pomenuti se koriste u okviru evropskih agencija (kao što su Italijanska svemirska agencija, Svemirska agencija Ujedinjenog Kraljevstva, i Evropska svemirska agencije) i {\em{Nacionalne vazduhoplovne i svemirske administracije}} (eng.~{\em{National Aeronautics and Space Administration} -- NASA}). Standarde koje koriste evropske agencije objavila je {\em{Evropska kooperacija za svemirsku standardizaciju}} (eng.~{\em{European Cooperation for Space Standardization} -- ECSS}).

\subsection{ECSS standardi}
\label{subsec:ECSS}
Evropska kooperacija za svemirsku standardizaciju je kooperativna inicijativa Evropske svemirske agencije (eng.~{\em{European Space Agency} -- ESA}), nacionalnih svemirskih agencija i evropskih industrijskih udruženja sa ciljem da razvijaju i održavaju zajedničke standarde. Standardizacija softvera u okviru ECSS se može podeliti u dve familije \cite{esa}:
\begin{itemize}
\item {\em{E-40}} -- grana koja se tiče inženjerstva
\item {\em{Q-80}} -- grana koja se tiče kvaliteta
\end{itemize}
Standardizacija verifikacije (i hardvera i softvera) svemirskih sistema\footnote{Svemirski sistem se može definisati kao sistem koji sadrži barem svemirski (eng.~{\em{space}}), zemaljski (eng.~{\em{ground}}) i segment za lanisranje (eng.~{\em{launch}}) \cite{ecss}.} je obuhvaćena standardom {\em{ECSS-E-ST-10-02C}} \cite{ecss}.

\subsubsection{Standard ECSS-E-ST-40C}
\label{subsubsec:ST-40C}

Standard ECSS-E-ST-40C je aktuelna revizija standarda ECSS-E-ST-40, objavljena 2009. godine. Tiče se {\em{softverskog inženjerstva}}, tj. softve\-ra koji je deo stabla proizvoda\footnote{Stablo proizvoda se definiše kao hijerarhijska podela proizvoda na uzastopne nivoe\cite{ecss}.} svemirskog sistema i koji se razvija kao deo svemirskog projekta. Obuhvata sve aspekte svemirskog softverskog inženjerstva -- definisanje zahteva, dizajn, proizvodnju, verifikaciju i validaciju, transfer, operacije i održavanje softvera \cite{ecss}.

Pod {\em{validacijom softvera}} se podrazumeva testiranje softverskog pro\-izvoda u odnosu na tehničku specifikaciju i definisane zahteve. Potrebno je da se validacijom potvrdi da su zahtevane funkcije i performanse ispravno i kompletno implementirane u završnom proizvodu. {\em{Verifikacija softvera}} treba da potvrdi da za svaku aktivnost postoje adekvatne specifikacije i ulazi, i da su izlazi aktivnosti ispravni i u skladu sa ulazima i specifikacijama \cite{ecss}. Verifikacija uključuje različite tehnike poput pregleda, inspekcija, testiranja, simulacije modela, i različite tipove analize, kao što su formalno dokazivanje i FTA \cite{ecss}.

%% Projekat prolazi kroz nekoliko revizija tokom svog razvoja. Neke od njih su preliminarna revizija dizajna (eng.~{\em{Preliminary design review}}), kritična revizija dizajna (eng.~{\em{Critical design review}}) i revizija kvalifikacije (eng.~{\em{Qualification review}}). Cilj preliminarne recenzije je pregled planova razvoja, verifikacije i validacije. Kritična revizija se vrši nakon kodiranja, testiranja, integracije i validacije, i njena svrha je pregled dizajna i kompletnosti izvršenog testiranja. Revizija kvalifikacije se vrši nakon završene validacije softvera, i njen rezultat je kvalifikovani softverski proizvod \cite{ecss40c}.

\subsubsection{Standard ECSS-Q-ST-80C}
\label{subsubsec:Q-ST-80C}

Standard ECSS-Q-ST-80C definiše skup zahteva za korišćenje prilikom razvoja i održavanja softvera u svemirskim sistemima sa ciljem osiguranja softverskog proizvoda. Osiguranje softvera se vrši da bi se obezbedila pouzdanost da je proizvod razvijen tako da radi ispravno i bezbedno u svom operativnom okruženju, i da pritom ispunjava dogovorene mere kvaliteta.
%% \footnote{Pod osiguranjem softverskog proizvoda se podrazumevaju aktivnosti, standardi, kontrole i procedure tokom životnog veka softverskog proizvoda koji uspostavljaju poverenje da isporučeni proizvod (ili softver koji utiče na kvalitet isporučenog proizvoda) ispunjava zahteve korisnika\cite{ecss}.}
Standard propisuje mere pomoću kojih se može osigurati pouzdanost i bezbednost kritičnog softvera. Neke od tih mera su: korišćenje softverskog dizajna ili metoda koje su uspešno izvođene u sličnim aplikacijama, ubacivanje funkcija za izolaciju kvarova i rukovanje kvarovima, tehnike odbrambenog programiranja (eng.~{\em{defensive programming}}) \cite{codeComplete} (kao što su verifikacija ulaza i provera konzistentnosti), korišćenje ``sigurnog podskupa'' programskih jezika \cite{safeLangSubset}, korišćenje formalnih dokaza \cite{rigSoftDev}, potpuna pokrivenost grana u kodu na nivou jedinica koda i prikupljanje i analiza statistike kvarova \cite{ecss}.
Kritični softver treba povrgnuti regresionom testiranju \cite{Myers.2012} nakon svake promene funkcionalnosti hardverske osnove i nakon promena alata koji direktno ili indirektno menjaju generisanje izvršnog koda. Standard takođe propisuje brisanje identifikovanog nedostižnog koda (eng.~{\em{unreachable code}}) \cite{Myers.2012} i razmatranje potrebe za ponovnom verifikacijom i validacijom.

\subsubsection{Standard ECSS-E-ST-02C}
\label{subsubsec:E-ST-02C}
Standard ECSS-E-ST-02C utvrđuje zahteve za verifikaciju proizvoda svemirskog sistema. U njemu se navodi da se verifikacija vrši pomoću jedne ili više od sledećih metoda verifikacije:
\begin{enumerate}
\item {\em{Testiranje}} --- merenje performansi i funkcija pod reprezentativnim simuliranim uslovima
\item {\em{Analiza}} --- izvođenje teorijskih ili empirijskih evaluacija korišćenjem tehnika u koje spadaju sistematična, statistička i kvalitativna analiza dizajna, modeliranje i računarske simulacije
\item {\em{Revizija dizajna}} --- korišćenje odobrenih zapisa ili dokaza koji nedvosmisleno pokazuju ispunjenost zahteva (primeri takvih zapisa su dokumenti i izveštaji o dizajnu, tehnički opisi i inženjerski crteži)
\item {\em{Inspekcija}} --- vizuelno odlučivanje o fizičkim karakteristikama, kao što su, na primer, usklađenost hardvera sa dokumentovanim crtežima i usklađenost softverskog izvornog koda sa standardima za kodiranje
\end{enumerate}
Redosled oslikava prioritet koji, u opštem slučaju, daje pouzdane rezultate \cite{ecss}.


\subsection{NASA propisi}
\label{subsec:NASA}

Nacionalna vazduhoplovna i svemirska administracija trenutno koristi {\em{AS9100}} međunardni standard vazduhoplovne i svemirske industrije objavaljen od strane {\em{SAE}}-e (Society of Automotive Engineers), koji predstavlja standardizovan sistem za upravljanje kvalitetom, razvijen za komercijalnu vazdušno-svemirsku industriju. Najnovija revizija tog standarda objavljena je 2016. godine i označava se sa {\em{AS9100D}}. Zasnovan je na {\em{ISO 9001}} \cite{iso} zahtevima za kvalitet sistema i definiše dodatne zahteve vezane za avionsku, svemirsku i odbrambenu industriju. Objavljen je pod imenom {\em{EN9100}} u Evropi \cite{NASAhandbook, sae}.

%% Životni ciklus NASA-inih projekata se sastoji iz faza o kojima se može više pročitati u \cite{NASAhandbook}. Ovde će biti navedene one faze u kojima Prilikom definisanja misije određuju se zahtevi vezani za verifikaciju, koji se ponovo razmatraju tokom faze definisanja sistema. Tokom faze preliminarnog dizajna se vrši evaluacija, validacija i verifikacija dizajna i formira se kompletan plan testiranja i verifikacije. U fazi finalnog dizajna se vrše validacija i verfikacija dizajna. Tokom faze izrade i integracije izvodi se verifikacija hardvera, verifikacija i validacija delova softvera, i verifikacija celokupnog sistema. 

%% Tokom životnog ciklusa NASA-inih projekata formira se plan verifikacije i validacije. Metode verifikacije koje se koriste su analiza, demonstracija, inspekcija i testiranje. {\em{Analiza}} podrazumeva korišćenje matematičkih modela i simulacija kao alata za analizu. {\em{Demonstracija}} je pokazivanje da korišćenje završnog proizvoda postiže određeni zahtev, bez prikupljanja detaljnih podataka. {\em{Inspekcija}} je vizuelno ispitivanje realizovanog proizvoda. {\em{Testiranje}} podrazumeva korišćenje završnog proizvoda za prikupljanje detaljnih podataka potrebnih za verifikaciju performansi kroz dalju analizu. Nakon verifikacije vrši se validacija proizvoda \cite{NASAhandbook}.

Osiguravanje softvera se vrši kroz nekoliko disciplina, kao što su verifikacija i validacija softvera (eng.~{\em{software V\&V}}), nezavisna verifikacija i validacija softvera (eng.~{\em{independent V\&V} -- IV\&V}), kvalitet softvera (eng.~{\em{software quality}}) i bezbednost softvera (eng.~{\em{software safety}}). V\&V softvera obezbeđuje usklađenost softvera sa zahtevima i davanje pravih rezultata od strane svih procesa. IV\&V pruža objektivno ispitivanje softver\-skih procesa i proizvoda kritičnih za misiju, proverava da li je razvijen pravi sistem i da li je razvijen ispravno. Kvalitet softvera obezbeđuje integrisanost kvaliteta u proizvod. Bezbednost softvera se fokusira na bezbednosno-kritičan softver, identifikuje, analizira, prati, ublažava i kontroliše opasan (eng.~{\em{hazardous}}) softver i funkcije softvera \cite{NASAsoftAssurance}.


\section{Zaključak}
\label{sec:zakljucak}
Opisani standardi i tehnike koji se primenjuju u razvoju bezbednosno-kritičnog softvera u avionskoj i svemirskoj industriji, pokazuju da se u poslednje vreme sve više pridaje značaj verifikaciji softvera zasnovanoj na formalnim metodama. Primenom ovakve vrste verifikacije u značajnoj meri se umanjuje potrebno vreme za verifikaciju u razvojnom procesu. Postoji osnovana mogućnost da će ona u potpunosti zameniti klasične metode testiranja, jer se pokazuje kao jedini način suočavanja sa dramatičnim povećanjem složenosti softvera. Kako bi se u potpunosti prešlo na ovakvu vrstu verifikacije postoji još izazova sa kojima se treba suočiti. Stoga je potrebno nastaviti istraživanja u ovom smeru, kao i razvijanje alata zasnovanih na formalnim metodama za komercijalne svrhe.


\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{seminarski} 
\bibliographystyle{plain}

\end{document}
