#!/bin/bash

###Input *.tex file passed as the command line argument
TEX="$1"
###Generate corresponding *.aux file by cutting off ".tex"
AUX="${TEX%.tex}.aux"

###bibtex##: Operate silently
SILENT=-terse

###pdflatex##:
#1 Do not wait user for an interaction
#2 Write detailed error message (filename also)
FLAGS="-interaction=nonstopmode -file-line-error"

###grep##: filter warnings only
WARN=".*:[0-9]*:.*\|warning"

pdflatex $FLAGS $TEX >& /dev/null
bibtex $SILENT $AUX
pdflatex $FLAGS $TEX >& /dev/null
pdflatex $FLAGS $TEX | grep -i "$WARN"
